const ReturnMessage = (result, message) => {
    return { result, message }
}

exports.SUCCESS = ReturnMessage(1, "Success")

exports.WRONG_KEY = ReturnMessage(0, "Out of version")

exports.NO_USER = ReturnMessage(0, "User not exist")

exports.DUP_USER = ReturnMessage(0, "User already exist")

exports.MAX_USER = ReturnMessage(0, "This site cann't add anymore user please contact your corporate")

exports.WRONG_UNIQ = ReturnMessage(0, "This account already login another device")

exports.DUP_PROJECT = ReturnMessage(0, "This project alias is already exits")

exports.NO_PROJECT = ReturnMessage(0, "This project alias is not available")

exports.NO_SITE = ReturnMessage(0, "This site code is not available")

exports.NO_SITE_PERMISSION = ReturnMessage(0, "You don't have permission for this site")

exports.OUT_OF_SERVICE = ReturnMessage(0, "Out of service")

exports.WRONG_PASSWORD = ReturnMessage(0, "Wrong password")

exports.NO_PERMISSION = ReturnMessage(0, "No permission")

exports.ALREADY_SITE = ReturnMessage(0, "Already add this site")