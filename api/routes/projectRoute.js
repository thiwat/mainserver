'use strict'
const projectController = require('../controllers/projectController')

module.exports = function(app) {
    app.route('/project')
        .post(projectController.createProject)

    app.route('/project/site')
        .post(projectController.createSite)

    app.route('/project/site/:siteCode')
        .put(projectController.updateSite)

    app.route('/siteregist')
        .post(projectController.siteRegist)

    app.route('/siteupdate')
        .post(projectController.siteUpdate)
}