'use strict'
const userController = require('../controllers/userController')

module.exports = function(app) {
    app.route('/login')
        .post(userController.login)

    app.route('/register')
        .post(userController.register)

    app.route('/renewpassword')
        .post(userController.changePassword)

    app.route('/changeimage')
        .post(userController.changeImage)

    app.route('/approveuser')
        .post(userController.approveUser)

    app.route('/addsite')
        .post(userController.addSite)

    app.route('/forgetpassword')
        .post(userController.genPassword)

    app.route('/deleteuser')
        .post(userController.deleteUser)
}