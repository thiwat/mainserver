'use strict'
const mongoose = require('mongoose')
const ProjectModel = mongoose.model('projects')
const SiteModel = mongoose.model('sites')
const { DUP_PROJECT, SUCCESS, NO_PROJECT, NO_SITE } = require('../../errors')
const { logger } = require('../../log')
const { clearSiteTemp, readSiteTemp } = require('../../utils/file')
const { validate } = require('../../utils/validate')

const createProject = async (req, res) => {
    const { name, alias } = req.body
    const error = validate({
        name: 'string',
        alias: 'string'
    }, req.body)
    if(error) {
        return res.status(400).json(error)
    }

    const result = await ProjectModel.findOne({ alias })
    if (result) {
        return res.status(422).json(DUP_PROJECT)
    }
    const id = await ProjectModel.countDocuments() + 1
    const createDate = new Date()
    const project = new ProjectModel({ id, name, alias, createDate })
    await project.save()
    logger('INFO', `Create project ${alias} Success`)
    return res.json({ ...SUCCESS, message: `Create project success with id ${id}` })
}

const createSite = async (req, res) => {
    const { siteName, siteCode, project, gateList, maxUser } = req.body
    const error = validate({
        siteName: 'string',
        siteCode: 'string',
        project: 'string',
        gateList: 'array',
        maxUser: 'number'
    }, req.body)

    if(error) {
        return res.status(400).json(error)
    }


    if (!await ProjectModel.findOne({ alias: project })) {
        return res.status(404).json(NO_PROJECT)
    }

    const id = await SiteModel.countDocuments() + 1
    const createDate = new Date()
    const site = new SiteModel({ id, name: siteName, code: siteCode, project, gateList, currentUser: 0, maxUser, createDate })
    await site.save()
    clearSiteTemp(siteCode)
    logger('INFO', `Create site ${siteCode} Success`)
    return res.json({ ...SUCCESS, message: `Create site success with id ${id}` })
}

const updateSite = async (req, res) => {
    const { siteName, gateList, maxUser } = req.body
    const { siteCode } = req.params

    if (!await SiteModel.findOne({ code: siteCode })) {
        return res.status(404).json(NO_SITE)
    }

    const updateDate = new Date()
    await SiteModel.updateOne({ code: siteCode }, { $set: { name: siteName, gateList, maxUser, updateDate } })
    logger('INFO', `Update site ${siteCode} success`)
    return res.json(SUCCESS)
}

const siteRegist = async (req, res) => {
    const { id, code, gateList } = req.body
    const site = await SiteModel.findOne({ id, code, gateList })
    if (!site) {
        return res.status(404).json(NO_SITE)
    }
    return res.json(SUCCESS)
}

const siteUpdate = async (req, res) => {
    const { code } = req.body
    const data = await readSiteTemp(code)
    return res.json({ ...SUCCESS, ...data })
}

module.exports = { createProject, createSite, updateSite, siteRegist, siteUpdate }