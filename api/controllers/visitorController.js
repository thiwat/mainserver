'use strict'
const mongoose = require('mongoose')
const UserModel = mongoose.model('users')
const VisitorModel = mongoose.model('visitors')
const { logger } = require('../../log')
const { genQrcodeMessage, genHashQrcode } = require('../../utils/hash')
const { SUCCESS, NO_SITE_PERMISSION, NO_PERMISSION, OUT_OF_SERVICE } = require('../../errors')
const { genQRCodeImage } = require('../../utils/qrcode')
const { saveToSiteTemp } = require('../../utils/file')

const qrcode = async (req, res) => {
    const { id, name, plate, duration, siteId, siteCode, project, hash } = req.body

    const user = await UserModel.findOne({ id, siteList: {$elemMatch: {id: siteId }}})
    if (!user) {
        return res.status(404).json( NO_SITE_PERMISSION )
    }

    if(genHashQrcode(id, name, plate, duration) !== hash) {
        return res.status(400).json( NO_PERMISSION )
    }
    
    let code 
    const TIME_STAMP = Math.floor(new Date() / 1000)
    for(let i=0; i<5; i++) {
        const temp = Math.floor(Math.random()*900000) + 100000
        const visitor = await VisitorModel.findOne({ code: temp, siteCode, expireTime: {$gt: TIME_STAMP}})
        if(!visitor) {
            code = temp
            break
        }
    }

    if(!code) {
        logger('ERR', 'Can not random code in 5 times')
        return res.status(500).json( OUT_OF_SERVICE )
    }

    const visitor = new VisitorModel({ userId: id, name, plate, code, siteCode, expireTime: TIME_STAMP + (duration * 60) })
    await visitor.save()

    const message = genQrcodeMessage(id, code)
    const qrcodeImage = await genQRCodeImage(message, project)

    await saveToSiteTemp(siteCode, 'qrcodeList', {code, userId: user.id, userName: user.name, visitorName: name, visitorPlate: plate, duration})

    return res.json({ ...SUCCESS, code, qrcode: qrcodeImage })
}

module.exports = { qrcode }