'use strict'
const mongoose = require('mongoose')
const UserModel = mongoose.model('users')
const SiteModel = mongoose.model('sites')
const { logger } = require('../../log')
const { SUCCESS, NO_USER, WRONG_UNIQ, DUP_USER, NO_SITE, MAX_USER, OUT_OF_SERVICE, WRONG_PASSWORD, ALREADY_SITE } = require('../../errors')
const { genHashPassword, genSalt } = require('../../utils/hash')
const { saveToUserTemp, saveToSiteTemp, readUserTemp, deleteUserTemp, saveUserImage, readUserImage } = require('../../utils/file')
const { existsSync } = require('fs')
const { sendEmail } = require('../../utils/mail')
const { validate } = require('../../utils/validate')

const login = async (req, res) => {
    const error = validate({
        phoneNumber: 'string',
        password: 'string'
    }, req.body)
    if (error) {
        return res.status(400).json(error)
    }
    const { phoneNumber, password, uniqueId } = req.body
    const user = await UserModel.findOne({ phoneNumber })
    if (!user) {
        return res.status(401).json(NO_USER)
    }

    if (user.uniqueId !== undefined && user.uniqueId != uniqueId) {
        logger('INFO', `${phoneNumber} try to login with wrong unique id`)
        return res.status(401).json(WRONG_UNIQ)
    }

    const [hashPassword, salt] = user.password.split(':')
    if (genHashPassword(password, salt) !== hashPassword) {
        logger('INFO', `${phoneNumber} try to login with wrong password`)
        return res.status(401).json(NO_USER)
    }

    const siteList = user.siteList
    for (let i = 0, max = siteList.length; i < max; i++) {
        const { gateList, name, code, project } = await SiteModel.findOne({ id: siteList[i].id })
        siteList[i].name = name
        siteList[i].code = code
        siteList[i].project = project
        siteList[i].gateList = gateList
    }

    if (user.uniqueId === undefined) {
        await UserModel.updateOne({ id: user.id }, { $set: { uniqueId } })
    }

    try {
        const image = await readUserImage(phoneNumber)
        return res.json({ ...SUCCESS, id: user.id, name: user.name, siteList, image })
    } catch (err) {
        console.log(err)
        return res.status(500).json(OUT_OF_SERVICE)
    }
}

const register = async (req, res) => {
    const error = validate({
        phoneNumber: 'string',
        password: 'string',
        name: 'string',
        email: 'string',
        image: 'string',
        siteCode: 'string',
        address: 'string'
    }, req.body)

    if (error) {
        return res.status(400).json(req.body)
    }

    const { phoneNumber, password, name, email, image, siteCode, address } = req.body
    if (!!await UserModel.findOne({ phoneNumber })) {
        return res.status(422).json(DUP_USER)
    }

    if (existsSync(`${process.env.NODE_DIR}/static/users/${phoneNumber}.json`)) {
        return res.status(422).json(DUP_USER)
    }

    const salt = genSalt(phoneNumber)
    const hashPassowrd = genHashPassword(password, salt)
    const storePassword = `${hashPassowrd}:${salt}`

    saveToUserTemp(phoneNumber, { password: storePassword, name, email, image, siteCode, address })
    saveToSiteTemp(siteCode, 'userRequestList', { name, email, phoneNumber })

    return res.json(SUCCESS)
}

const addSite = async (req, res) => {
    const error = validate({
        phoneNumber: 'string',
        password: 'string',
        siteCode: 'string',
        address: 'string'
    }, req.body)

    if (error) {
        return res.status(400).json(error)
    }

    const { phoneNumber, password, siteCode, address, uniqueId } = req.body

    const user = await UserModel.findOne({ phoneNumber })
    if (!user) {
        return res.status(404).json(NO_USER)
    }

    const [userPassword, userSalt] = user.password.split(':')
    if (genHashPassword(password, userSalt) !== userPassword) {
        return res.status(401).json(WRONG_PASSWORD)
    }

    if (user.uniqueId !== uniqueId) {
        return res.status(401).json(WRONG_UNIQ)
    }

    const site = await SiteModel.findOne({ code: siteCode })
    if (!site) {
        return res.status(404).json(NO_SITE)
    }

    const { name, email, siteList } = user

    const checkAlready = siteList
        .filter(item => item.id === site.id)
    if (checkAlready.length) {
        return res.status(404).json(ALREADY_SITE)
    }

    if (await existsSync(`${process.env.NODE_DIR}/static/users/${phoneNumber}.json`)) {
        return res.status(422).json(DUP_USER)
    }

    saveToUserTemp(phoneNumber, { address })
    saveToSiteTemp(siteCode, 'userRequestList', { name, email, phoneNumber })

    return res.json(SUCCESS)
}

const changePassword = async (req, res) => {
    const { id, oldPassword, newPassword, uniqueId } = req.body

    const user = await UserModel.findOne({ id })

    if (user.uniqueId !== uniqueId) {
        logger('INFO', `${user.phoneNumber} try to change password with wrong unique id`)
        return res.status(401).json(WRONG_UNIQ)
    }

    const [hashPassword, salt] = user.password.split(':')
    if (genHashPassword(oldPassword, salt) !== hashPassword) {
        logger('INFO', `${user.phoneNumber} try to change password with wrong password`)
        return res.status(401).json(WRONG_PASSWORD)
    }

    const newSalt = genSalt(user.phoneNumber)
    const newHashPassword = genHashPassword(newPassword, newSalt)
    const storePassword = `${newHashPassword}:${newSalt}`

    await UserModel.updateOne({ id }, { $set: { password: storePassword } })
    return res.json(SUCCESS)
}

const changeImage = async (req, res) => {
    const { phoneNumber, image, uniqueId } = req.body

    const user = await UserModel.findOne({ phoneNumber })
    if (user.uniqueId !== uniqueId) {
        logger('INFO', `user ${phoneNumber} try to change image with wrong unique id`)
        return res.status(401).json(WRONG_UNIQ)
    }

    for (let i = 0, max = user.siteList.length; i < max; i++) {
        const { id } = user.siteList[i]
        const { code } = await SiteModel.findOne({ id })
        await saveToSiteTemp(code, 'changeImageList', { phoneNumber, image })
    }


    const imagePath = `${process.env.NODE_DIR}/static/images/users/${phoneNumber}.png`
    await saveUserImage(imagePath, image)
    return res.json(SUCCESS)
}

const approveUser = async (req, res) => {
    const { siteCode, email, phoneNumber, status } = req.body
    const path = `${process.env.NODE_DIR}/static/users/${phoneNumber}.json`
    if (!existsSync(path)) {
        return res.status(404).json(NO_USER)
    }

    if (status === 'reject') {
        await deleteUserTemp(phoneNumber)
        sendEmail(email, `Your account is reject from site ${siteCode}`)
        return res.json(SUCCESS)
    }

    const site = await SiteModel.findOne({ code: siteCode })
    if (!site) {
        return res.status(404).json(NO_SITE)
    }

    if (site.currentUser + 1 > site.maxUser) {
        logger('INFO', `Site ${siteCode} full user`)
        return res.status(400).json(MAX_USER)
    }

    await SiteModel.updateOne({ code: siteCode }, { $inc: { currentUser: 1 } })

    const user = await UserModel.findOne({ phoneNumber })
    if (user) {
        const { address } = await readUserTemp(phoneNumber)
        const { id, name, siteList } = user
        const image = await readUserImage(phoneNumber)
        const updateDate = Math.floor(new Date() / 1000)

        siteList.push({ id: site.id, address })
        await UserModel.updateOne({ phoneNumber }, { $set: { siteList, updateDate } })
        logger('INFO', `Update user ${phoneNumber} success`)
        sendEmail(email, `Your account alredy to use at site ${siteCode}`)
        return res.json({ ...SUCCESS, id, phoneNumber, name, email, address, image })
    } else {
        const { password, name, image, address } = await readUserTemp(phoneNumber)
        const imagePath = `${process.env.NODE_DIR}/static/images/users/${phoneNumber}.png`
        const siteList = { id: site.id, address }
        const createDate = Math.floor(new Date() / 1000)
        const id = await UserModel.countDocuments() + 1

        const newUser = new UserModel({ id, phoneNumber, password, name, email, imagePath, siteList, createDate })
        await newUser.save()

        saveUserImage(imagePath, image)

        logger('INFO', `Add user ${phoneNumber} success`)
        sendEmail(email, `Your account alredy to use at site ${siteCode}`)
        return res.json({ ...SUCCESS, id, phoneNumber, name, email, address, image })
    }
}

const genPassword = async (req, res) => {
    const { phoneNumber, uniqueId } = req.body

    const user = await UserModel.findOne({ phoneNumber })
    if (!user) {
        return res.status(404).json(NO_USER)
    }

    if (user.uniqueId !== uniqueId) {
        return res.status(401).json(WRONG_UNIQ)
    }

    const newPassword = Math.floor(Math.random() * 900000) + 100000
    const salt = genSalt(phoneNumber)
    const hashPassword = genHashPassword(newPassword, salt)
    const storePassword = `${hashPassword}:${salt}`

    await UserModel.updateOne({ phoneNumber }, { $set: { password: storePassword } })

    sendEmail(user.email, `Your new password is ${newPassword}`)
    return res.json(SUCCESS)
}

const deleteUser = async (req, res) => {
    const { id, siteId } = req.body
    const user = await UserModel.findOne({ id })
    if (!user) {
        return res.status(404).json(NO_USER)
    }
    const siteList = user.siteList
        .filter(item => item.id !== siteId)
    await UserModel.updateOne({ id }, { $set: { siteList } })
    return res.json(SUCCESS)
}

module.exports = { login, register, changePassword, changeImage, approveUser, addSite, genPassword, deleteUser }