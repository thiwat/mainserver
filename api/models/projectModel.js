const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProjectSchema = new Schema ({
    id: Number,
    name: String,
    alias: String
})

module.exports = mongoose.model('projects', ProjectSchema)