const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema ({
    id: Number,
    phoneNumber: String,
    password: String,
    name: String,
    email: String,
    imagePath: String,
    siteList: Array,
    uniqueId: String,
    createDate: Date,
    updateDate: Date
})

module.exports = mongoose.model('users', UserSchema)