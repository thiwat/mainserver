const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VisitorSchema = new Schema ({
    userId: Number,
    code: String,
    name: String,
    plate: String,
    expireTime: Number,
    siteCode: String,
})

module.exports = mongoose.model('visitors', VisitorSchema)