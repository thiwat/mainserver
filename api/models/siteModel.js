const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SiteSchema = new Schema ({
    id: Number,
    name: String,
    code: String,
    project: String,
    gateList: Array,
    currentUser: Number,
    maxUser: Number,
    createDate: Date,
    updateDate: Date
})

module.exports = mongoose.model('sites', SiteSchema)