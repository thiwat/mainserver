const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const { logger, requestLogger } = require('./log')
const { checkSecret } = require('./utils/auth')
const { createInitialFolder } = require('./utils/file')

require('dotenv').config()
process.env.NODE_DIR = __dirname

const app = express()
const port = process.env.PORT

require('./api/models/userModel')
require('./api/models/projectModel')
require('./api/models/siteModel')
require('./api/models/visitorModel')

function gracefulShutdown() {
    logger('INFO', 'Got terminate signal, Server is shutting down...')
    server.close(() => {
        logger('INFO', 'Shutdown server success')
        process.exit()
    })

    const timeout = 10 * 1000 // 10 second
    setTimeout(() => {
        logger('ERR', 'Shutdown server unsuccess, Force shutdown server')
        process.exit(1)
    }, timeout)
}

process.on('SIGTERM', gracefulShutdown)
process.on('SIGINT', gracefulShutdown)

mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection err : '))

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(bodyParser.json({ limit: '50mb' }))

const projectRoute = require('./api/routes/projectRoute')
projectRoute(app)

app.use(requestLogger)
app.use(checkSecret)

const userRoute = require('./api/routes/userRoute')
const visitorRoute = require('./api/routes/visitorRoute')
userRoute(app)
visitorRoute(app)

createInitialFolder()

const server = app.listen(port)
logger('INFO', `Server is ready for listen and serve with port ${port}`)

module.exports = app