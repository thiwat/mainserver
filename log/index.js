const { saveLog } = require('../utils/file')

const logger = (type, message) => {
    console.log(`[${getTimeStamp()}] ${type} - ${message}`)
    saveLog(`[${getTimeStamp()}] ${type} - ${message}`)
}

const requestLogger = (req, res, next) => {
    const { method, originalUrl, connection } = req
    console.log(`[${getTimeStamp()}] REQ - ${connection.remoteAddress} ${method} ${decodeURI(originalUrl)}`)
    saveLog(`[${getTimeStamp()}] REQ - ${connection.remoteAddress} ${method} ${decodeURI(originalUrl)}`)
    next()
}

const getTimeStamp = () => {
    return new Date()
        .toISOString()
        .replace(/T/, ' ')
        .replace(/\..+/, '')
}

module.exports = { logger, requestLogger }