# Main Server

## Config
before run the server
- Set the SECRET_KEY in the .env file

## Command
`docker-compose up --build -d` command for start the server  
`docker-compose stop` for stop the server  
`docker logs --tail 100 -f mainserver_core_1` to see logs
`docker exec -it {container_name} {command}` to run the command in the container

## Structure
* utils - general functions
    * validate - use for validate uesr input
    * auth - use for check the secret key
    * hash - use for hash message
    * qrcode - use for gen qrcode image
    * file - use for save data to local device
* static - store the file that use for web page && temp file
* log - use for store the log
* errors - list of the error message and code
* api
    * controllers - store the end point
    * routes - map the url with the end point
    * modle - database model
