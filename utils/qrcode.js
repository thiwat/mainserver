'use strict'

const { read, AUTO } = require('jimp')
const { toDataURL } = require('qrcode')
const { existsSync } = require('fs')

const genQRCodeImage = async (message, project) => {
    const base64 = await toDataURL(message, { scale: 8 })
    const qrcodeData = base64.split('base64,')[1]
    const image = await read(Buffer.from(qrcodeData, 'base64' ))

    let path = `${process.env.NODE_DIR}/static/images/CIT.png`
    if( existsSync(`${process.env.NODE_DIR}/static/images/${project}.png`)) {
        path = `${process.env.NODE_DIR}/static/images/${project}.png`
    }
    const logo = await read(path)

    const logoSize = 72
    const resizeLogo = logo.resize(logoSize, logoSize)
    const { width } = image.bitmap
    const logoPosition = (width - logoSize) / 2

    image.composite(resizeLogo, logoPosition, logoPosition)
    const res = await image.getBase64Async(AUTO)
    return res.split('base64,')[1]
}

module.exports = { genQRCodeImage }