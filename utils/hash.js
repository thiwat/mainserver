'use strict'
const crypto = require('crypto')

const hashSha1 = (message) => {
    return crypto.createHash('sha1').update(message).digest('hex')
}

const genHashPassword = (password, salt) => {
    return hashSha1(`${password}${salt}`)
}

const genSalt = (phoneNumber) => {
    return hashSha1(`${phoneNumber}VV${new Date()}`)
}

const genQrcodeMessage = (id, code) => {
    const message = hashSha1(`QR${id}CODE${code}VERILY`)
    return `${message}${code}${id}`
}

const genHashQrcode = (id, name, plate, duration) => {
    return hashSha1(`${id}VERILY${name}QR${plate}CODE${duration}`)
}

module.exports = { genHashPassword, genSalt, genQrcodeMessage, genHashQrcode }