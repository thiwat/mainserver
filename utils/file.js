'use strict'
const { writeFileSync, readFileSync, unlinkSync, appendFileSync, existsSync, mkdirSync } = require('fs')

const saveToSiteTemp = async (site, type, message) => {
    const path = `${process.env.NODE_DIR}/static/sites/${site}.json`
    const data = await JSON.parse(readFileSync(path))
    data[type].push(message)
    await writeFileSync(path, JSON.stringify(data))
}

const saveToUserTemp = async (phoneNumber, user) => {
    const path = `${process.env.NODE_DIR}/static/users/${phoneNumber}.json`
    await writeFileSync(path, JSON.stringify(user))
}

const readSiteTemp = async (site) => {
    const path = `${process.env.NODE_DIR}/static/sites/${site}.json`
    const data = await JSON.parse(readFileSync(path))
    clearSiteTemp(site)
    return data
}

const readUserTemp = async (phoneNumber) => {
    const path = `${process.env.NODE_DIR}/static/users/${phoneNumber}.json`
    const data = JSON.parse(await readFileSync(path))
    deleteUserTemp(phoneNumber)
    return data
}

const deleteUserTemp = async (phoneNumber) => {
    const path = `${process.env.NODE_DIR}/static/users/${phoneNumber}.json`
    await unlinkSync(path)
}

const clearSiteTemp = async (site) => {
    const path = `${process.env.NODE_DIR}/static/sites/${site}.json`
    await writeFileSync(path, JSON.stringify({
        userRequestList: [],
        qrcodeList: [],
        changeImageList: []
    }))
}

const saveUserImage = async (path, image) => {
    await writeFileSync(path, image, { encoding: 'base64' })
}

const readUserImage = async (phoneNumber) => {
    const path = `${process.env.NODE_DIR}/static/images/users/${phoneNumber}.png`
    return await readFileSync(path, { encoding: 'base64' })
}

const saveLog = async (message) => {
    const path =`${process.env.NODE_DIR}/log/logs.txt`
    await appendFileSync(path, message + '\n')
}

const createInitialFolder = async () => {
    const path =`${process.env.NODE_DIR}/static/`
    const folders = ['sites', 'users']
    for(const folder of folders) {
        if(!await existsSync(path + folder)) {
            await mkdirSync(path + folder)
        }
    }
}

module.exports = { saveToSiteTemp, saveToUserTemp, readSiteTemp, clearSiteTemp, readUserTemp, deleteUserTemp, saveUserImage, readUserImage, saveLog, createInitialFolder }