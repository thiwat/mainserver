const _ = require('lodash')

const validate = (rule, params) => {
  // return error message when not match
  // return false it the params match the rule
  for (const key in rule) {
    if (typeof params[key] !== rule[key]) {
      return { message: `${key} require a ${rule[key]} type, but got a ${typeof params[key]}` }
    }
    if (_.isEmpty(params[key])) {
      return { message: `${key} field must not be empty` }
    }
  }
  return false
}

module.exports = {
  validate
}